package gerprint.gerprint;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import com.sun.pdfview.PagePanel;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import javax.swing.JFrame;

public class PdfTest {

    public static void main(String[] args) {
        try {
            JFrame frame = new JFrame("PDF Test");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            PagePanel panel = new PagePanel();
            frame.add(panel);
            frame.pack();
            frame.setVisible(true);
            File file = new File("test/teste.pdf");
            RandomAccessFile raf = new RandomAccessFile(file, "r");
            FileChannel channel = raf.getChannel();
            ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
            PDFFile pdffile = new PDFFile(buf);
            PDFPage page = pdffile.getPage(0);
            panel.showPage(page);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public PdfTest() {
        try {
            JFrame frame = new JFrame("PDF Test");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            PagePanel panel = new PagePanel();
            frame.add(panel);
            frame.pack();
            frame.setVisible(true);
            File file = new File("test/teste.pdf");
            RandomAccessFile raf = new RandomAccessFile(file, "r");
            FileChannel channel = raf.getChannel();
            ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
            PDFFile pdffile = new PDFFile(buf);
            PDFPage page = pdffile.getPage(0);
            panel.showPage(page);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}