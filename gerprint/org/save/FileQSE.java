/*
 * 
 */
package gerprint.gerprint.org.save;

import gerprint.gerprint.org.eng.Relatorio;
import java.io.File;
import java.io.IOException;
import java.util.zip.ZipException;
import win.MZip;

/**
 *
 * @author Plinio
 */
public class FileQSE extends QSE {

    private File file;
    private MZip mzip;
    //

    public FileQSE(MZip mzip) throws IOException, ClassNotFoundException {
        super((Relatorio) mzip.readObject(PATH_RELATORIO));
    }

    public void load(File savedFile) throws ZipException, IOException, ClassNotFoundException {
        file = savedFile;
        mzip = new MZip(savedFile);
    }

    public void save() throws IOException, ClassNotFoundException {
        mzip.saveObject(getRelatorio(), PATH_RELATORIO);
    }

    public File getFile() {
        return file;
    }

    public MZip getMzip() {
        return mzip;
    }
}
