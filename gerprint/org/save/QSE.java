/*
 * ARQUIVOS NOS ARQUIVOS QSE.
 * 
 * CLASS - OBS.
 * Relatorio.class - RELATÓRIO PAGINADO.
 * 
 */
package gerprint.gerprint.org.save;

import gerprint.gerprint.org.eng.Relatorio;

/**
 *
 * @author Plinio
 */
public class QSE {

    private Relatorio relatorio;
    //
    public final static String PATH_RELATORIO = "RELATORIO";
    public final static String EXTENSAO_DO_ARQUIVO = "qseprint";

    public QSE(Relatorio relatorio) {
        this.relatorio = relatorio;
    }

    public Relatorio getRelatorio() {
        return relatorio;
    }

    public void setRelatorio(Relatorio relatorio) {
        this.relatorio = relatorio;
    }
}
