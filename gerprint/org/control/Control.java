package gerprint.gerprint.org.control;

import gerprint.gerprint.org.eng.RelatorioView;
import gerprint.gerprint.org.save.FileQSE;
import gerprint.gerprint.org.save.QSE;
import gerprint.gerprint.pdfcreator.PDFCreator;
import java.io.File;
import java.io.IOException;
import win.HD;
import win.MZip;

public abstract class Control {

    /*
     * Tempo máximo de espera em segundos.
     */
    public static final int TEMPO_MAXIMO_DE_ESPERA = 10; // SEGUNDOS
    /*
     * Objetos státicos para manipulação.
     */
    private static QSE qse;
    private static FileQSE fqse;

    /*
     * Abrir arquivos .QSEPRINT
     */
    public static void open(String path) {
        File file = new File(path);
        open(file);
    }

    public static void open(File file) {

        String[] split = file.getName().split("\\.");
        if (split.length <= 0) {
            System.out.println("Falha em identificar extensão do arquivo!");
        } else {
            String nome = split[0];
            String extensao = split[1];
            if (extensao.toLowerCase().equals(QSE.EXTENSAO_DO_ARQUIVO)) {

                try {
                    MZip zip = new MZip(file);
                    fqse = new FileQSE(zip);
                    qse = (QSE) fqse;
                } catch (Exception ex) {
                    System.out.println("Falha ao abrir o arquivo .QSE");
                    ex.printStackTrace();
                }


                try {
                    RelatorioView view = new RelatorioView(fqse);
                    view.start();
                } catch (Exception ex) {
                    System.out.println("O Arquivo foi alterado.");
                    ex.printStackTrace();
                }


            } else {
                System.out.println("Extensão do arquivo não foi reconhecida.");
            }

        }
    }

    /*
     * Necessita do PDFcreator.exe
     * @param PDFCreator - Caminho do PDFCreator
     */
    public static void convertDOCtoPDF(PDFCreator pdfcreator, String oldPath, String newPath) throws IOException, InterruptedException {
        File oldFile = new File(oldPath);
        File newFile = new File(newPath);
        convertDOCtoPDF(pdfcreator, oldFile, newFile);
    }

    /*
     * Necessita do PDFcreator.exe
     * @param PDFCreator - Caminho do PDFCreator
     */
    public static void convertDOCtoPDF(PDFCreator pdfcreator, File oldFile, File newFile) throws IOException, InterruptedException {
        System.out.println("Criando \".PDF\"");
        Process process = Runtime.getRuntime().exec(
                "\""
                + pdfcreator.getPDFCreator().getAbsolutePath()
                + "\" /NoStart /PF\""
                + oldFile.getAbsolutePath()
                + "\"");
        File pdffile = new File(pdfcreator.getOutputPath() + "\\" + oldFile.getName().split("\\.")[0] + ".pdf");
        /*
         * Aguarda a criação do PDF e conta 10 segundos, se demorar muito cancela.
         */
        Boolean status = true;
        int contador = 0;
        while (status) {
            if (pdffile.exists()) {
                System.out.println("Movendo .PDF");
                HD.move(pdffile, newFile);
                status = false;
            } else {
                contador++;
                Thread.sleep(1000);
                if (contador == TEMPO_MAXIMO_DE_ESPERA) {
                    System.out.println("Tempo máximo de espera para criação do .PDF excedido, operação foi cancelada.");
                    Thread.interrupted();
                }
            }
        }
    }
}