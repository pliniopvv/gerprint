/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gerprint.gerprint.org.eng;

import com.sun.pdfview.PDFPage;
import java.io.Serializable;

/**
 *
 * @author Plinio
 */
public class Pagina implements Serializable {

    public static final Integer NORMAL = 1;
    public static final Integer COLORIDA = 2;
    public static final Integer PRETA_E_BRANCA = 3;
    private PDFPage pagina;
    private Integer numero;
    private Integer cor;

    /*
     * @param numero - Requer o número da página;
     */
    public Pagina(PDFPage pagina, Integer numero) {
        this.pagina = pagina;
        this.numero = numero;
        this.cor = Pagina.NORMAL;
    }

    /*
     * Métodos personalisados
     */
    //
    /*
     * Métodos Automáticos
     */
    public PDFPage getPDFPagina() {
        return pagina;
    }

    public void setPagina(PDFPage pagina) {
        this.pagina = pagina;
    }

    public Integer getCor() {
        return cor;
    }

    public void setCor(Integer cor) {
        this.cor = cor;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }
}
