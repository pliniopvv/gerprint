package gerprint.gerprint.org.eng;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import java.io.Serializable;

public class Relatorio implements Serializable {

    private Pagina[] paginas;

    /* 
     * param @n - Número de páginas que o documento possui.
     */
    public Relatorio(int numerosDePaginas) {
        this.paginas = new Pagina[numerosDePaginas];
        for (int i = 1; i <= numerosDePaginas; i++) {
            this.paginas[i - 1] = new Pagina(null, i);
        }
    }

    public Relatorio(PDFPage[] pdf) {
        int numerosDePaginas = pdf.length;
        this.paginas = new Pagina[numerosDePaginas];
        for (int i = 1; i <= numerosDePaginas; i++) {
            this.paginas[i - 1] = new Pagina(pdf[i - 1], i);
        }
    }

    public Relatorio(PDFFile pdf) {
        int numerosDePaginas = pdf.getNumPages();
        this.paginas = new Pagina[numerosDePaginas];
        for (int i = 1; i <= numerosDePaginas; i++) {
            this.paginas[i - 1] = new Pagina(pdf.getPage(i), i);
        }
    }

    /*
     * Métodos personalisados
     */
    public void setPageColor(int numero, int cor) {
        Pagina a = this.paginas[numero - 1];
        a.setCor(cor);
    }

    public void setCCode(String codigoDeImpressao) {
        String[] nPaginas = codigoDeImpressao.split(";");
        int nPagina;
        for (String pagina : nPaginas) {
            if (pagina.indexOf("-") == -1) {
                nPagina = Integer.parseInt(pagina);
                this.getPagina(nPagina).setCor(Pagina.COLORIDA);
            } else {
                String[] divPaginas = pagina.split("-");
                int paginaInicial = Integer.parseInt(divPaginas[0]);
                int paginaFinal = Integer.parseInt(divPaginas[1]);
                while (paginaInicial <= paginaFinal) {
                    this.getPagina(paginaInicial).setCor(Pagina.COLORIDA);
                    paginaInicial++;
                }
            }
        }
    }

    public void setPBCode(String codigoDeImpressao) {
        String[] nPaginas = codigoDeImpressao.split(";");
        int nPagina;
        for (String pagina : nPaginas) {
            if (pagina.indexOf("-") == -1) {
                nPagina = Integer.parseInt(pagina);
                this.getPagina(nPagina).setCor(Pagina.PRETA_E_BRANCA);
            } else {
                String[] divPaginas = pagina.split("-");
                int paginaInicial = Integer.parseInt(divPaginas[0]);
                int paginaFinal = Integer.parseInt(divPaginas[1]);
                while (paginaInicial <= paginaFinal) {
                    this.getPagina(paginaInicial).setCor(Pagina.PRETA_E_BRANCA);
                    paginaInicial++;
                }
            }
        }
    }

    public void setNtoPB() {
        int nPaginas = count();
        Pagina pagina;
        for (int i = 1; i <= nPaginas; i++) {
            pagina = getPagina(i);
            if (pagina.getCor() == Pagina.NORMAL) {
                pagina.setCor(Pagina.PRETA_E_BRANCA);
            }
        }
    }

    public void setNtoC() {
        int nPaginas = count();
        Pagina pagina;
        for (int i = 1; i <= nPaginas; i++) {
            pagina = getPagina(i);
            if (pagina.getCor() == Pagina.NORMAL) {
                pagina.setCor(Pagina.COLORIDA);
            }
        }
    }

    public int countCPages() {
        int count = 0;
        for (Pagina pagina : paginas) {
            if (pagina.getCor() == Pagina.COLORIDA) {
                count++;
            }
        }
        return count;
    }

    public int countPBPages() {
        int count = 0;
        for (Pagina pagina : paginas) {
            if (pagina.getCor() == Pagina.PRETA_E_BRANCA) {
                count++;
            }
        }
        return count;
    }

    public int countNPages() {
        int count = 0;
        for (Pagina pagina : paginas) {
            if (pagina.getCor() == Pagina.NORMAL) {
                count++;
            }
        }
        return count;
    }
    /*
     * Métodos necessários
     */

    public Pagina getPagina(int numero) {
        return this.paginas[numero - 1];
    }

    public int count() {
        return this.paginas.length;
    }

    public String getCode(int Cor) {
        String retorno = "";
        Pagina paginaSelecionada; // Facilita para debugar
        for (int i = 1; i <= this.count(); i++) {
            paginaSelecionada = this.getPagina(i);
            if (paginaSelecionada.getCor() == Cor) {
                if (retorno.equals("")) {
                    retorno = this.getPagina(i).getNumero().toString();
                } else {
                    retorno += ";" + this.getPagina(i).getNumero();
                }
            }
        }
        return retorno;
    }
    /*
     * Métodos automáticos
     */

    public Pagina[] getPaginas() {
        return paginas;
    }

    public void setPagina(Pagina[] pagina) {
        this.paginas = pagina;
    }
}
