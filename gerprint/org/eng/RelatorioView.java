/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gerprint.gerprint.org.eng;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import com.sun.pdfview.PagePanel;
import gerprint.gerprint.org.save.FileQSE;
import gerprint.gerprint.org.save.QSE;
import java.awt.BorderLayout;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JTextField;
import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import win.ClipBoard;

/**
 *
 * @author Plinio
 */
public class RelatorioView extends javax.swing.JFrame {

    private QSE qse;
    private FileQSE fqse;
    private Relatorio relatorio;

    public Relatorio getPaginacao() {
        return relatorio;
    }

    public void setPaginacao(Relatorio relatorio) {
        this.relatorio = relatorio;
    }

    /**
     * Creates new form Print
     */
    public RelatorioView(Relatorio relatorio) {
        this.relatorio = relatorio;
        initComponents();
        initPage();
        buildTable();
    }

    public RelatorioView(QSE qse) {
        this.qse = qse;
        this.relatorio = qse.getRelatorio();
        initComponents();
        initPage();
        buildTable();
    }

    public RelatorioView(FileQSE fqse) {
        this.fqse = fqse;
        this.qse = (QSE) fqse;
    }

    public void start() {
        setVisible(true);
    }
    private PagePanel panel;
    private PDFFile pdffile;
    // Paginação
    private int cursor = 1;

    public void initPage() {
        try {
            panel = new PagePanel();
            PDFPanel.add(panel, BorderLayout.CENTER);
            pack();
            File file = new File("test/teste.pdf");
            RandomAccessFile raf = new RandomAccessFile(file, "r");
            FileChannel channel = raf.getChannel();
            ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
            pdffile = new PDFFile(buf);
            int nPages = pdffile.getNumPages();
            for (int i = 1; i <= nPages; i++) {
                LivroPDF.add(pdffile.getPage(i));
            }
            showPage(1);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RelatorioView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RelatorioView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private ArrayList<PDFPage> LivroPDF = new ArrayList<>();

    public PDFPage getPage(int num) {
        return relatorio.getPagina(num).getPDFPagina();
    }

    public void showPage(int num) {
        PDFPage SelectedPage = getPage(num);
        panel.showPage(SelectedPage);
        PDFPanel.repaint();
    }

    public void doNext() {
        cursor++;
        if (cursor == relatorio.count() + 1) {
            cursor = 1;
        }
        showPage(cursor);
    }

    public void doPrev() {
        cursor--;
        if (cursor <= 0) {
            cursor = relatorio.count();
        }
        showPage(cursor);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        scrol_table = new javax.swing.JScrollPane();
        panel_codigos = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        tf_color_code = new javax.swing.JTextField();
        tf_preta_code = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btn_copy_color_code = new javax.swing.JButton();
        btn_copy_preta_code = new javax.swing.JButton();
        PDFPanel = new javax.swing.JPanel();
        backPage = new javax.swing.JButton();
        forwardPage = new javax.swing.JButton();
        barra_menu = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        menu_salvar = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gerenciador de Impressão V1.0");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        scrol_table.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        panel_codigos.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 153, 153), 1, true));

        jLabel1.setText("Código");

        tf_color_code.setEditable(false);
        tf_color_code.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tf_color_code.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_color_codeActionPerformed(evt);
            }
        });

        tf_preta_code.setEditable(false);
        tf_preta_code.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tf_preta_code.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_preta_codeActionPerformed(evt);
            }
        });

        jLabel2.setText("Coloridas:");

        jLabel3.setText("Pretas:");

        btn_copy_color_code.setBackground(new java.awt.Color(102, 102, 255));
        btn_copy_color_code.setText("Copiar");
        btn_copy_color_code.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btn_copy_color_code.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_copy_color_codeActionPerformed(evt);
            }
        });

        btn_copy_preta_code.setBackground(new java.awt.Color(102, 102, 255));
        btn_copy_preta_code.setText("Copiar");
        btn_copy_preta_code.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout panel_codigosLayout = new javax.swing.GroupLayout(panel_codigos);
        panel_codigos.setLayout(panel_codigosLayout);
        panel_codigosLayout.setHorizontalGroup(
            panel_codigosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_codigosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panel_codigosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_codigosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tf_color_code, javax.swing.GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
                    .addComponent(tf_preta_code))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panel_codigosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_copy_color_code, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE)
                    .addComponent(btn_copy_preta_code, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panel_codigosLayout.setVerticalGroup(
            panel_codigosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_codigosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_codigosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panel_codigosLayout.createSequentialGroup()
                        .addGroup(panel_codigosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tf_color_code, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_copy_color_code, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel_codigosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tf_preta_code, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_copy_preta_code, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panel_codigosLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PDFPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        PDFPanel.setLayout(new java.awt.BorderLayout());

        backPage.setText("<<<<");
        backPage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backPageActionPerformed(evt);
            }
        });

        forwardPage.setText(">>>>");
        forwardPage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                forwardPageActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrol_table, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panel_codigos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PDFPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(backPage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(forwardPage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(PDFPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 514, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(backPage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(forwardPage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panel_codigos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(scrol_table))
                .addContainerGap())
        );

        jMenu1.setText("Sistema");

        menu_salvar.setText("Salvar");
        menu_salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_salvarActionPerformed(evt);
            }
        });
        jMenu1.add(menu_salvar);

        barra_menu.add(jMenu1);

        setJMenuBar(barra_menu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        setSize(new java.awt.Dimension(782, 707));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void tf_color_codeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tf_color_codeActionPerformed
        ClipBoard.setStringToClipboard(tf_preta_code.getText());
        JOptionPane.showMessageDialog(this,
                "O código para impressão das páginas PRETAS E BRANCAS foi copiado com sucesso",
                "Código copiado!",
                JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_tf_color_codeActionPerformed

    private void tf_preta_codeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tf_preta_codeActionPerformed
    }//GEN-LAST:event_tf_preta_codeActionPerformed

    private void btn_copy_color_codeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_copy_color_codeActionPerformed
        ClipBoard.setStringToClipboard(tf_color_code.getText());
        JOptionPane.showMessageDialog(this,
                "O código para impressão das páginas COLORIDAS foi copiado com sucesso",
                "Código copiado!",
                JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_btn_copy_color_codeActionPerformed

    private void backPageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backPageActionPerformed
        doPrev();
    }//GEN-LAST:event_backPageActionPerformed

    private void forwardPageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_forwardPageActionPerformed
        doNext();
    }//GEN-LAST:event_forwardPageActionPerformed

    private void menu_salvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_salvarActionPerformed
        try {
            if (fqse == null) {
                JOptionPane.showMessageDialog(this, "ERRO!", "Esta funcionalidade ainda não foi implementada!", JOptionPane.ERROR_MESSAGE);
            } else {
                fqse.save();
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERRO!", "Atenção ocorreu o erro: \n" + ex.getMessage(), JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            System.out.println("Erro desconhecido! favor verificar!");
            ex.printStackTrace();
        }
    }//GEN-LAST:event_menu_salvarActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel PDFPanel;
    private javax.swing.JButton backPage;
    private javax.swing.JMenuBar barra_menu;
    private javax.swing.JButton btn_copy_color_code;
    private javax.swing.JButton btn_copy_preta_code;
    private javax.swing.JButton forwardPage;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JMenuItem menu_salvar;
    private javax.swing.JPanel panel_codigos;
    private javax.swing.JScrollPane scrol_table;
    private javax.swing.JTextField tf_color_code;
    private javax.swing.JTextField tf_preta_code;
    // End of variables declaration//GEN-END:variables
    /*
     * Cor das células;
     */
    DefaultTableCellRenderer celulasCinzas = new DefaultTableCellRenderer() {
        public void setValue(Object value) {
            setBackground(new Color(238, 238, 238));
            setForeground(Color.BLACK);
            setHorizontalAlignment(JLabel.CENTER);

            //outras alterações entram aqui...
            super.setValue(value);
        }
    };
    PaginacaoTableModel modelo_tabela;
    JTable tabela;

    private void buildTable() {
        modelo_tabela = new PaginacaoTableModel(this.relatorio);
        tabela = new JTable(modelo_tabela);
        tabela.addMouseListener(new tabClickListener(tf_color_code, tf_preta_code));

        /*
         * Aplicando coloração na célula.
         */

        TableColumn tc = tabela.getColumn("Nº");
        tc.setCellRenderer(celulasCinzas);

        scrol_table.setViewportView(tabela);

        /*
         * Mostrar códigos na abertura do App.
         */

        Relatorio a = getPaginacao();
        tf_color_code.setText(a.getCode(Pagina.COLORIDA));
        tf_preta_code.setText(a.getCode(Pagina.PRETA_E_BRANCA));
    }

    private class tabClickListener implements MouseListener {

        private JTextField tf_c;
        private JTextField tf_pb;

        public tabClickListener(JTextField tf_c, JTextField tf_pb) {
            this.tf_c = tf_c;
            this.tf_pb = tf_pb;
        }

        @Override
        public void mouseClicked(MouseEvent me) {
        }

        @Override
        public void mousePressed(MouseEvent me) {
        }

        @Override
        public void mouseReleased(MouseEvent me) {
            Relatorio a = getPaginacao();
            this.tf_c.setText(a.getCode(Pagina.COLORIDA));
            this.tf_pb.setText(a.getCode(Pagina.PRETA_E_BRANCA));
        }

        @Override
        public void mouseEntered(MouseEvent me) {
        }

        @Override
        public void mouseExited(MouseEvent me) {
        }
    }
}
