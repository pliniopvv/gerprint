/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gerprint.gerprint.org.eng;

import gerprint.gerprint.org.eng.Pagina;
import gerprint.gerprint.org.eng.Relatorio;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Plinio
 */
public class PaginacaoTableModel extends AbstractTableModel {

    private Relatorio paginacao;

    public PaginacaoTableModel(Relatorio paginacao) {
        this.paginacao = paginacao;
    }

    public Class getColumnClass(int col) {
        if (col == 0) {
            return String.class;
        } else {
            return Boolean.class;
        }
    }

    @Override
    public int getRowCount() {
        return this.paginacao.count();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int col) {
        switch (col) {
            case 0:
                return "Nº";
            case 1:
                return "N";
            case 2:
                return "C";
            case 3:
                return "PB";
            default:
                return "ERRO";
        }
    }

    @Override
    public Object getValueAt(int row, int col) {

        /*
         * Inicia o trabalho com a Classe Pagina.java
         */
        //        Converção do vetor;
        int row_f = row + 1;

        Relatorio paginacao = this.paginacao;
        Pagina pagina = paginacao.getPagina(row_f);

        if (col == 0) {
            return row_f;
        } else if (col == 1) {
            if (pagina.getCor() == Pagina.NORMAL) {
                return true;
            } else {
                return false;
            }
        } else if (col == 2) {
            if (pagina.getCor() == Pagina.COLORIDA) {
                return true;
            } else {
                return false;
            }
        } else if (col == 3) {
            if (pagina.getCor() == Pagina.PRETA_E_BRANCA) {
                return true;
            } else {
                return false;
            }
        }
        return "ERRO";
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        if (col == 0) {
            return false;
        } else {
            return true;
        }
    }

    public void setValueAt(Object value, int row, int col) {
        int row_f = row + 1;
        Pagina pagina = this.paginacao.getPagina(row_f);

        if (pagina.getCor() != col) {
            pagina.setCor(col);
        }
        fireTableDataChanged();
    }
}
