/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gerprint.gerprint.org.eng;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import java.util.ArrayList;

/**
 *
 * @author Plinio
 */
public class RelatorioThread extends Thread {

    private PDFFile pdffile;
    private ArrayList<PDFPage> LivroPDF;
    private int page = 1;
    private RelatorioThreadView view;
    private boolean run = false;

    public RelatorioThread(PDFFile pdffile, ArrayList<PDFPage> LivroPDF, RelatorioThreadView view) {
        super();
        this.LivroPDF = LivroPDF;
        this.pdffile = pdffile;
        this.view = view;
    }

    @Override
    public void run() {
        this.run = true;
        synchronized (LivroPDF) {
            view.start();
            int nPages = pdffile.getNumPages();
            for (page = 1; page <= nPages; page++) {
                LivroPDF.add(pdffile.getPage(page));
                view.doNext();
                view.finish();
                this.run = false;
            }
        }
    }

    public int getCursorPage() {
        return page;
    }

    public PDFPage[] getPages() {
        return LivroPDF.toArray(new PDFPage[LivroPDF.size()]);
    }

    public int getNumPages() {
        return pdffile.getNumPages();
    }

    public boolean isRun() {
        return this.run;
    }
}
