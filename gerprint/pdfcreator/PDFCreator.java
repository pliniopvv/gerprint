/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gerprint.gerprint.pdfcreator;

import java.io.File;

/**
 *
 * @author Plinio
 */
public final class PDFCreator {

    private File pdfc;
    private String output;

    /*
     * Construtores
     */
    public PDFCreator(String PDFCreatorPath, String pathOutputPadrao) {
        this.pdfc = new File(PDFCreatorPath);
        this.output = pathOutputPadrao;
    }

    public PDFCreator(File PDFCreator, String outputPadrao) {
        this.pdfc = PDFCreator;
        this.output = outputPadrao;
    }

    /*
     * Setters & Getters padrões;
     */
    public File getPDFCreator() {
        return pdfc;
    }

    public void setPDFCreator(File PDFcreator) {
        this.pdfc = PDFcreator;
    }

    public String getOutputPath() {
        return output;
    }

    public void setOutputPath(String output) {
        this.output = output;
    }
}
