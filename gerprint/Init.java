/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gerprint.gerprint;

import gerprint.gerprint.org.eng.Relatorio;

/**
 *
 * @author Plinio
 */
public class Init {

    public static void main(String[] args) {

        try {
            Relatorio r = new Relatorio(Integer.valueOf(args[0]));
            r.setCCode(args[1]);
            r.setPBCode(args[2]);

            System.out.println("COMEÇO -------");
            System.out.println("Páginas coloridas: " + r.countCPages());
            System.out.println("Páginas pretas: " + r.countPBPages());
            System.out.println("Páginas não-paginadas: " + r.countNPages());
            System.out.println("Total: " + r.count());
            System.out.println("------- FIM");
        } catch (NullPointerException ex) {
            System.out.println("INFO:");
            System.out.println("init numPages CodeColor CodeMono");
            System.out.println("--");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
